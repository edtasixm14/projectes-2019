# Projectes 2019

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2018-2019

#### M14 Projecte

1) Parveen Gangas: **Ldap Avançat**

https://github.com/parveen1/project-parveen 


2) Dani Cano: **Ldap Gssapi**

https://github.com/LdapGessapi/LdapGessapi


3) Jose Otero: **Utilitats de seguretat**

https://gitlab.com/isx41005342/seguridad-informatica 


4) Eric Escribà: **Gandhi Reload**

https://github.com/xReve/greload 


5) Franlin Colque: **Grafana**

https://github.com/isx27423760/projecte-franlin 


6) Aïtor Galilea: **Migració del sistema**

https://gitlab.com/a.galilea/upgrading-debian/

Como al parecer a GIT no le gustan los archivos de más de 1 GB, iré subiendo copias de las máquinas virtuales a MEGA:
https://mega.nz/#F!FZ9TTCrb!9Xf9ajp85UGgYp4ZSgLRIQ






